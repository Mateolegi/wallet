import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';

import { UserService } from '../../providers';
import { MainPage } from '../';
import { User } from '../../models/user';
import { UserStatusCode } from '../../providers/user/user';
import { SplashScreen } from '@ionic-native/splash-screen';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: { name: string, email: string, password: string } = {
    name: '',
    email: '',
    password: ''
  };

  // Our translated text strings
  private signupErrorString: string;
  private signupExistsString: string;
  private fieldsRequiredString: string;

  constructor(public navCtrl: NavController,
      public userService: UserService,
      public toastCtrl: ToastController,
      public translateService: TranslateService,
      private splashScreen: SplashScreen) {
    this.getMessages();
  }

  /**
   * Get messages from translation service
   * @private
   * @memberof SignupPage
   */
  private getMessages() {
    this.translateService.get('SIGNUP_ERROR').subscribe((value) => {
      this.signupErrorString = value;
    });
    this.translateService.get('SIGNUP_EXISTS').subscribe((value) => {
      this.signupExistsString = value;
    });
    this.translateService.get('FIELDS_REQUIRED').subscribe((value) => {
      this.fieldsRequiredString = value;
    });
  }

  async doSignup() {
    this.splashScreen.show();
    try {
      if (this.validateFields()) {
        // Attempt to login in through our User service
        let user: User = {
          name: this.account.name,
          email: this.account.email,
          password: this.account.password
        };
        let response = await this.userService.signup(user);
        if (response.statusCode === UserStatusCode.USER_CREATED) {
          this.navCtrl.push(MainPage);
        } else {
          let message = response.statusCode === UserStatusCode.USER_EXISTS
            ? this.signupExistsString : this.signupErrorString;
          let toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'top'
          });
          toast.present();
        }
      } else {
        this.toastCtrl.create({
          message: this.fieldsRequiredString,
          duration: 3000,
          position: 'top'
        }).present();
      }
    } catch (error) {
      console.log(JSON.stringify(error));
      // Unable to sign up
      let toast = this.toastCtrl.create({
        message: this.signupErrorString,
        duration: 3000,
        position: 'top'
      });
      toast.present();
    }
    this.splashScreen.hide();
  }

  /**
   * Check fields are not empty
   * @private
   * @returns {boolean}
   * @memberof SignupPage
   */
  private validateFields(): boolean {
    this.account.name = this.account.name.trim();
    this.account.email = this.account.email.trim();
    this.account.password = this.account.password.trim();
    return this.account.name !== '' && this.account.email !== ''
      && this.account.password !== '';
  }
}
