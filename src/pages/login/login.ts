import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';

import { UserService } from '../../providers';
import { SplashScreen } from '@ionic-native/splash-screen';
import { UserStatusCode } from '../../providers/user/user';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: { email: string, password: string } = {
    email: '',
    password: ''
  };

  // Our translated text strings
  private loginErrorString: string;
  private fieldsRequiredString: string;
  private userNotExistsString: string;
  private serverErrorString: string;

  constructor(public navCtrl: NavController,
      public userService: UserService,
      public toastCtrl: ToastController,
      public translateService: TranslateService,
      private splashScreen: SplashScreen) {
    this.getMessages();
  }

  /**
   * Get messages from translation service
   * @private
   * @memberof LoginPage
   */
  private getMessages() {
    this.translateService.get('LOGIN_ERROR').subscribe((value) => {
      this.loginErrorString = value;
    });
    this.translateService.get('USER_NOT_EXISTS').subscribe((value) => {
      this.userNotExistsString = value;
    });
    this.translateService.get('FIELDS_REQUIRED').subscribe((value) => {
      this.fieldsRequiredString = value;
    });
    this.translateService.get('SERVER_ERROR').subscribe((value) => {
      this.serverErrorString = value;
    });
  }

  // Attempt to login in through our User service
  async doLogin() {
    this.splashScreen.show();
    if (this.validateFields()) {
      try {
        let response = await this.userService.login({
          email: this.account.email,
          password: this.account.password
        });
        if (response.statusCode === UserStatusCode.SUCCESS) {
          this.toastCtrl.create({
            message: 'success',
            duration: 3000,
            position: 'top'
          }).present();
        } else if (response.statusCode === UserStatusCode.INVALID_CREDENTIALS) {
          this.toastCtrl.create({
            message: this.loginErrorString,
            duration: 3000,
            position: 'top'
          }).present();
        } else if (response.statusCode === UserStatusCode.USER_NOT_EXISTS) {
          this.toastCtrl.create({
            message: this.userNotExistsString,
            duration: 3000,
            position: 'top'
          }).present();
        } else {
          this.toastCtrl.create({
            message: this.serverErrorString,
            duration: 3000,
            position: 'top'
          }).present();
        }
      } catch (error) {
        console.error(error);
        this.toastCtrl.create({
          message: this.serverErrorString,
          duration: 3000,
          position: 'top'
        }).present();
      }
    } else {
      this.toastCtrl.create({
        message: this.fieldsRequiredString,
        duration: 3000,
        position: 'top'
      }).present();
    }
    this.splashScreen.hide();
  }

  async getUsers() {
    let users = await this.userService.getAll();
    alert(JSON.stringify(users));
  }

  async deleteUsers() {
    await this.userService.deleteAll();
  }

  /**
   * Check fields are not empty
   * @private
   * @returns {boolean}
   * @memberof LoginPage
   */
  private validateFields(): boolean {
    this.account.email = this.account.email.trim();
    this.account.password = this.account.password.trim();
    return this.account.email !== '' && this.account.password !== '';
  }
}
