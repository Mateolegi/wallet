import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';

import { DatabaseProvider } from '../database/database';
import { User } from '../../models/user';
import queries from './../database/queries.json';

/**
 * Most apps have the concept of a User. This is a simple provider
 * with stubs for login/signup/etc.
 *
 * This User provider makes calls to our database at the `login` and `signup` endpoints.
 *
 * By default, it expects `login` and `signup` to return a JSON object of the shape:
 *
 * ```json
 * {
 *   statusCode: SUCCESS
 *   status: 'success',
 *   user: {
 *     // User fields your app needs, like "id", "name", "email", etc.
 *   }
 * }
 * ```
 *
 * If the `status` field is not `success`, then an error is detected and returned.
 * @export
 * @class UserService
 */
@Injectable()
export class UserService {
  _user: User;

  constructor(private database: DatabaseProvider) { }

  /**
   * Search a user in the database and compare password
   * to login
   * @param {User} accountInfo
   * @returns {Promise<UserResponse>}
   * @memberof UserService
   */
  async login(accountInfo: User): Promise<UserResponse> {
    try {
      let possibleUser = await this.validateExistingUser(accountInfo.email);
      if (possibleUser !== null) {
        if (possibleUser.password === accountInfo.password) {
          let response = {
            statusCode: UserStatusCode.SUCCESS,
            status: 'success',
            user: possibleUser
          };
          this._loggedIn(response);
          return response;
        } else {
          return {
            statusCode: UserStatusCode.INVALID_CREDENTIALS,
            status: 'error',
            user: null
          };
        }
      } else {
        return {
          statusCode: UserStatusCode.USER_NOT_EXISTS,
          status: 'error',
          user: null
        };
      }
    } catch (error) {
      return {
        statusCode: UserStatusCode.SERVER_ERROR,
        status: 'error',
        user: null
      };
    }
  }

  /**
   * Create a user
   * @param {User} accountInfo
   * @memberof UserService
   */
  async signup(accountInfo: User): Promise<UserResponse> {
    let possibleUser = await this.validateExistingUser(accountInfo.email);
    if (possibleUser === null) {
      try {
        let data = await this.database.executeSql(queries.queries.user.insert, [
          accountInfo.name,
          accountInfo.email,
          accountInfo.password
        ]);
        if (data.rowsAffected > 0) {
          console.log('UserService', 'User created', JSON.stringify(data));
          let newUser = await this.database.executeSql(queries.user.selectById, [data.insertId]);
          let response = {
            statusCode: UserStatusCode.USER_CREATED,
            status: 'success',
            user: newUser.rows.item(0)
          };
          this._loggedIn(response);
          return response;
        }
      } catch (error) {
        console.error(JSON.stringify(error));
        return {
          statusCode: UserStatusCode.SERVER_ERROR,
          status: 'error',
          user: null
        };
      }
    } else {
      return {
        statusCode: UserStatusCode.USER_EXISTS,
        status: 'error',
        user: null
      };
    }
  }

  async getAll(): Promise<User[]> {
    try {
      let data = await this.database.executeSql(queries.user.selectAll, []);
      let arr: User[] = [];
      for (let index = 0; index < data.rows.length; index++) {
        arr.push({
          id: data.rows.item(index).id,
          name: data.rows.item(index).name,
          email: data.rows.item(index).email,
          password: data.rows.item(index).password
        });
      }
      return arr;
    } catch (error) {
      throw error;
    }
  }

  async deleteAll(): Promise<void> {
    try {
      await this.database.executeSql('DELETE FROM USERS', []);
    } catch (error) {
      throw error;
    }
  }

  /**
   * Validate user exists by email
   * @private
   * @param {string} email
   * @returns {Promise<User>}
   * @memberof UserService
   */
  private async validateExistingUser(email: string): Promise<User> {
    try {
      let data = await this.database.executeSql(queries.user.selectByEmail, [email]);
      if (data && data.rows.length > 0) {
        return {
          id: data.rows.item(0).id,
          name: data.rows.item(0).name,
          email: data.rows.item(0).email,
          password: data.rows.item(0).password
        };
      } else {
        return null;
      }
    } catch (error) {
      return null;
    }
  }

  /**
   * Log the user out, which forgets the session
   * @memberof UserService
   */
  logout(): void {
    this._user = null;
  }

  /**
   * Process a login/signup response to store user data
   * @param {UserResponse} resp
   * @memberof UserService
   */
  _loggedIn(resp: UserResponse): void {
    this._user = resp.user;
  }
}

/**
 * @export
 * @interface UserResponse
 */
export interface UserResponse {
  statusCode: UserStatusCode;
  status: string;
  user: User;
}

/**
 * @export
 * @enum {number}
 */
export enum UserStatusCode {
  USER_CREATED,
  SUCCESS,
  INVALID_CREDENTIALS,
  SERVER_ERROR,
  USER_NOT_EXISTS,
  USER_EXISTS
}
