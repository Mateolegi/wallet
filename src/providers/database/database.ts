import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

/*
  Generated class for the DatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DatabaseProvider {

  private db: SQLiteObject;
  private isOpen: boolean;

  constructor(private storage: SQLite) {
    this.createDatabase();
  }

  private async createDatabase() {
    try {
      if (!this.isOpen) {
        this.storage = new SQLite();
        this.db = await this.storage.create({
          name: 'data.db',
          location: 'default'
        });
        await this.db.executeSql(`
          CREATE TABLE IF NOT EXISTS USERS (
            ID INTEGER PRIMARY KEY AUTOINCREMENT,
            NAME VARCHAR(100),
            EMAIL VARCHAR(100),
            PASSWORD VARCHAR(300)
          )`, []);
        this.isOpen = true;
      }
    } catch (error) {
      console.error(JSON.stringify(error));
      throw error;
    }
  }

  public async executeSql(sql: string, params: any) {
    try {
      if (!this.isOpen) await this.createDatabase();
      return await this.db.executeSql(sql, params);
    } catch (error) {
      console.error(JSON.stringify(error));
      throw error;
    }
  }
}
